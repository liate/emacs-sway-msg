(use-modules (guix packages)
             (guix gexp)
             (guix build-system copy)
             (gnu packages web)
             (gnu packages wm)
             (gnu packages emacs)
             ((guix licenses) #:prefix l:))

(package
  (name "emacs-sway-msg")
  (version "0.1")
  (source (local-file (dirname (current-filename))
                      #:recursive? #t))
  (build-system copy-build-system)
  (arguments
   (list
    #:install-plan ''(("." "share/emacs/site-lisp/" #:include-regexp (".*\\.el"))
                      ("emacs-sway-msg-proxy" "bin/"))))
  (propagated-inputs (list jq))
  (synopsis "Some scripts for combining emacs and sway")
  (description
   "Scripts for controlling emacs with sway keybinds.  Currently requires swaymsg and
emacsclient in your PATH.")
  (home-page "none")
  (license l:cc-by4.0))
