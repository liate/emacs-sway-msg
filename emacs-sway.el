;; -*- lexical-binding: t -*-
;;; A series of functions for unifying sway and emacs movement
;;; From [[https://sqrtminusone.xyz/posts/2021-10-04-emacs-i3/]]

(defun i/emacs-sway-msg (command)
  (pcase command
    ((rx bos "focus")
     (i/emacs-sway-windmove
      (intern (elt (split-string command) 1))))
    ((rx bos "move")
     (i/emacs-sway-move-window
      (intern (elt (split-string command) 1))))
    ("split h" (evil-window-split))
    ("split v" (evil-window-vsplit))
    ("kill" (evil-quit))
    (- (sway-msg command))))

(defvar i/swaymsg-command "swaymsg")
(defun sway-msg (&rest args)
  (apply #'start-process "emacs-sway" nil i/swaymsg-command args))

(defun i/emacs-sway-windmove (dir)
  (let ((other-window (windmove-find-other-window dir)))
    (if (or (null other-window) (window-minibuffer-p other-window))
        (sway-msg "focus" (symbol-name dir))
      (windmove-do-window-select dir))))


(defun i/emacs-sway-direction-exists-p (dir)
  (some (lambda (dir)
          (let ((win (windmove-find-other-window dir)))
            (and win (not (window-minibuffer-p win)))))
        (pcase dir
          ('width '(left right))
          ('height '(up down)))))

(defun i/emacs-sway-move-window (dir)
  (let ((other-window (windmove-find-other-window dir))
        (other-direction (i/emacs-sway-direction-exists-p
                          (pcase dir
                            ('up 'width)
                            ('down 'width)
                            ('left 'height)
                            ('right 'height)))))
    (cond
     ((and other-window (not (window-minibuffer-p other-window)))
      (window-swap-states (selected-window) other-window))
     (other-direction
      (evil-move-window dir))
     (t (sway-msg "move" (symbol-name dir))))))

(provide 'emacs-sway)
